## ABN AMRO ASSESSMENT - Test Automation Guide

This project includes CRUD operations verifications of GitLab Issues API  
**Scenarios included are :**
1. Create a new issue
2. Read created issue
3. Update existing issue
4. Delete existing issue
Additionally includes below negative/edge cases
5. Create a new issue with invalid issue type
6. Update existing issue with invalid state type
7. Delete non-existing issue

**Tools and technologies used :**
 - Rest-assured 
 - Maven
 - TestNG
 - Lombok
 - Jackson

**Steps to execute tests :**
1. mvn clean install - build and download dependencies 
2. locate 'GitLabIssues.xml' (src\test\resources\TestNGTestRunners\GitLabIssues.xml) execute E2E test scenarios

**Additional information on the project structure :**

com.abn.gitlab.test.BaseResponseDTO - Base map of a generic API response
com.abn.gitlab.test.TestBase - Before suite to start up by loading configs

 - com.abn.gitlab.utils.constants.RelativeURLs - Relative URLs of endpoints
 - com.abn.gitlab.utils.data.Config - Setting up configs to load in the test base
 - com.abn.gitlab.utils.data.HeaderProvider - Load headers from Header template
 - com.abn.gitlab.utils.rest.APIServicesBase - Basic request template of CRUD operations

 - com.abn.gitLab.responses.DTO - Map request and response DTOs

 - com.abn.gitLab.services.GitLabIssueService - Service layer to decouple tests with base CRUD requests

 - com.abn.gitLab.test.IssuesTests - Test cases

**Prepared by:** Jayanga Windsor


