package com.abn.gitlab.test;

import com.abn.gitlab.utils.data.Config;
import com.abn.gitlab.utils.data.HeaderProvider;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.annotations.BeforeSuite;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public abstract class TestBase {

	private static String templateBodyPath;
	private static String templateHeaderPath;
	protected Map<String, Object> headers;
	protected Map<String, JSONObject> value;
	protected static String enviroment;
	@BeforeSuite(alwaysRun=true)
	public static void startUp() throws Exception {

		try {
			Config.setConfigFilePath("/Config/configuration.properties");
			templateHeaderPath = Config.getProperty("template.header.path");
			enviroment=Config.getProperty("enviroment");
			if(templateHeaderPath != null)
				HeaderProvider.setHeaderProviderFilePath(templateHeaderPath);
		} catch (Exception e) {
			throw e;
		}
	}


}
