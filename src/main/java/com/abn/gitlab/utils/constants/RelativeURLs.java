package com.abn.gitlab.utils.constants;

public class RelativeURLs {
    private RelativeURLs() {
		    throw new IllegalStateException("RelativeURLs class");
		  }

    public static final String GET_ISSUES = "/issues";
    public static final String CREATE_ISSUE = "/projects/{id}/issues";
    public static final String UPDATE_ISSUE = "/projects/{id}/issues/{issue_ID}";
    public static final String DELETE_ISSUE = "/projects/{id}/issues/{issue_ID}";


}
