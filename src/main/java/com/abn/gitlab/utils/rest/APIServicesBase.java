package com.abn.gitlab.utils.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import io.restassured.response.Response;
import java.util.Map;
import static io.restassured.RestAssured.given;

public abstract class APIServicesBase extends RestUtil {

    protected ObjectMapper objectMapper = new ObjectMapper();
    public APIServicesBase() {
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
    }
    public Response postRequest(Map<String, Object> headers, String baseURI, String relativeURI, Map<String, String> query) {

        try {
            setBaseURI(baseURI);
            setBasePath(relativeURI);
            Response response =
                    given()
                            .queryParams(query)
                            .headers(headers)
                            .when()
                            .post();

            // Clear Base Path
            resetBaseURI();
            resetBasePath();

            return response;

        } catch (Exception ex) {
            throw ex;
        }
    }

    public Response putRequest(Map<String, Object> headers, String baseURI, String relativeURI, Map<String, String> query) {

        try {
            setBaseURI(baseURI);
            setBasePath(relativeURI);

            Response response =
                    given()
                            .queryParams(query)
                            .headers(headers)
                            .when()
                            .put();

            // Clear Base Path
            resetBaseURI();
            resetBasePath();

            return response;

        } catch (Exception ex) {
            throw ex;
        }
    }

    public Response deleteRequest(Map<String, Object> headers, String baseURI, String relativeURI) {

        try {
            setBaseURI(baseURI);
            setBasePath(relativeURI);

            Response response =
                    given()
                            .headers(headers)
                            .when()
                            .delete();

            resetBaseURI();
            resetBasePath();

            return response;

        } catch (Exception ex) {
            throw ex;
        }
    }

    public Response getRequest(Map<String, Object> headers, String baseURI, String relativeURI) {

        try {
            setBaseURI(baseURI);
            setBasePath(relativeURI);
            Response response =
                    given()
                            .headers(headers)
                            .when()
                            .get();

            resetBaseURI();
            resetBasePath();

            return response;

        } catch (Exception ex) {
            throw ex;
        }
    }

}
