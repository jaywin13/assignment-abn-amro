package com.abn.gitLab.services;

import com.abn.gitLab.responses.NewIssueResponseDTO;
import com.abn.gitlab.utils.constants.RelativeURLs;
import com.abn.gitlab.utils.data.Config;
import com.abn.gitlab.utils.rest.APIServicesBase;
import io.restassured.response.Response;

import java.util.Map;

public class GitLabIssueService extends APIServicesBase {

    private NewIssueResponseDTO newIssueResponseDTO;
    private NewIssueResponseDTO[] newIssueResponseDTOS;


    private String baseURI;

    public GitLabIssueService() {

        this.baseURI = Config.getProperty("gitLabService.baseurl");
    }


    public NewIssueResponseDTO CreateNewIssues(Map<String, Object> headers, String ProjectID, Map<String, String> query ) throws Exception {

        try {
            Response response = postRequest(headers, baseURI, RelativeURLs.CREATE_ISSUE.replace("{id}",ProjectID),query);
            if (response.statusCode() == 201)
                newIssueResponseDTO = objectMapper.readValue(response.asString(), NewIssueResponseDTO.class);
            else
                newIssueResponseDTO = new NewIssueResponseDTO();

            newIssueResponseDTO.setResponse(response);
            
        } catch (Exception e) {
            throw new Exception("Failed to Create  Issue  " + e.getLocalizedMessage());
        }
        return newIssueResponseDTO;
    }

    public NewIssueResponseDTO UpdateIssue(Map<String, Object> headers, String ProjectID, String IssueID, Map<String, String> query) throws Exception {
        try {
            Response response = putRequest(headers, baseURI, RelativeURLs.UPDATE_ISSUE.replace("{id}", ProjectID).replace("{issue_ID}", IssueID), query);

            if (response.statusCode() == 200)
                newIssueResponseDTO = objectMapper.readValue(response.asString(), NewIssueResponseDTO.class);
            else
                newIssueResponseDTO = new NewIssueResponseDTO();

            newIssueResponseDTO.setResponse(response);
            return newIssueResponseDTO;

        } catch (Exception e) {
            throw new Exception("Failed to Update  Issue  " + e.getLocalizedMessage());
        }
    }

    public NewIssueResponseDTO[] getIssues(Map<String, Object> headers) throws Exception {
        try {
            Response response = getRequest(headers, baseURI, RelativeURLs.GET_ISSUES);

            if (response.statusCode() == 200)

                newIssueResponseDTOS =  objectMapper.readValue(response.asString(), NewIssueResponseDTO[].class);

            else
                newIssueResponseDTO = new NewIssueResponseDTO();


            return newIssueResponseDTOS;

        } catch (Exception e) {
            throw new Exception("Failed : Retrive Issue List " + e.getLocalizedMessage());
        }
    }
    
    public NewIssueResponseDTO[] getIssue(Map<String, Object> headers, String IssueID) throws Exception {
        try {
            Response response = getRequest(headers, baseURI, RelativeURLs.GET_ISSUES.replace("{issue_ID}", IssueID));

            if (response.statusCode() == 200)

                newIssueResponseDTOS =  objectMapper.readValue(response.asString(), NewIssueResponseDTO[].class);

            else
                newIssueResponseDTO = new NewIssueResponseDTO();


            return newIssueResponseDTOS;

        } catch (Exception e) {
            throw new Exception("Failed : Retrive Issue List " + e.getLocalizedMessage());
        }
    }


    public Response deleteIssue (Map<String, Object> headers, String ProjectID, String IssueID) throws Exception {
        try {

            return deleteRequest(headers, baseURI, RelativeURLs.DELETE_ISSUE.replace("{id}", ProjectID).replace("{issue_ID}", IssueID));


        } catch (Exception e) {
            throw e;
        }
    }



}
