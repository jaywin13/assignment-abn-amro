package com.abn.gitLab.test;

import com.abn.gitLab.responses.NewIssueResponseDTO;
import com.abn.gitLab.services.GitLabIssueService;
import com.abn.gitlab.test.TestBase;
import com.abn.gitlab.utils.data.HeaderProvider;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author jayanga
 * Description: This test class contains an end to end flow
 * which includes create, read, update and delete issues on gitlab
 */

public class IssuesTests extends TestBase {
    private NewIssueResponseDTO newIssueResponseDTO;
    private NewIssueResponseDTO[] newIssueResponseDTOS;
    private GitLabIssueService gitLabIssueService;
    private String header = "Header0";
    private String projectID = "35370969";
    private  String issueTitle = "New Test Issue - ABN AMRO";
    private String issueID = null;
    private int newIssueId = 0;

    @BeforeClass
    public void serviceSetUp() throws Exception {

        try {

            newIssueResponseDTO = new NewIssueResponseDTO();
            gitLabIssueService = new GitLabIssueService();


        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Scenario: Create a new issue
     * @throws Exception
     */

    @Test(priority = 1)
    public void createNewIssue() throws Exception {

        try {
            Map<String, String> query = new HashMap<String, String>();
            query.put("title", issueTitle);
            query.put("labels", "bug");
            headers = HeaderProvider.getHeaders(header);
            newIssueResponseDTO = gitLabIssueService.CreateNewIssues(headers, projectID, query);
            Assert.assertEquals(newIssueResponseDTO.getStatusCode(), 201);
            newIssueId = newIssueResponseDTO.getIid();
            issueID = String.valueOf(newIssueId);

            
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Negative/Edge case
     * Scenario: Trying to create an issue with an invalid issue type
     * Valid types: issue, incident, test_case
     * @throws Exception
     */
    @Test(priority = 1)
    public void createNewIssueWithInvalidIssueType() throws Exception {

        try {
            Map<String, String> query = new HashMap<String, String>();
            query.put("title", issueTitle);
            query.put("labels", "bug");
            query.put("issue_type", "test1");
            headers = HeaderProvider.getHeaders(header);
            newIssueResponseDTO = gitLabIssueService.CreateNewIssues(headers, projectID, query);
            Assert.assertEquals(newIssueResponseDTO.getStatusCode(), 400);
            Assert.assertTrue(newIssueResponseDTO.getResponse().jsonPath().getString("error").contains("does not have a valid value"));


        } catch (Exception e) {
            throw e;
        }
    }


    /**
     * Scenario: Verify that the new issue creation was successful
     * @throws Exception
     */
    @Test(priority = 2)
    public void getSpecificIssue() throws Exception {

        try {
            boolean flag = false;
            headers = HeaderProvider.getHeaders(header);
            newIssueResponseDTOS = gitLabIssueService.getIssue(headers,issueID);
            for(NewIssueResponseDTO a: newIssueResponseDTOS){
                if(a.getIid() == newIssueId) {
                    flag = true;
                    break;
                }
            }
            Assert.assertTrue(flag,issueID+" : has NOT been created successfully!");
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Scenario: Update the created issue title with a new title
     * @throws Exception
     */
    @Test(priority = 3)
    public void updateIssueTitle() throws Exception {

        try {
            Map<String, String> query = new HashMap<String, String>();
            query.put("title", "Issue Title Updated");

            headers = HeaderProvider.getHeaders(header);
            newIssueResponseDTO = gitLabIssueService.UpdateIssue(headers, projectID, issueID, query);
            Assert.assertEquals(newIssueResponseDTO.getStatusCode(), 200);

        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Negative/Edge Case
     * Scenario: Update the issue state with an invalid value
     * Valid states : close, reopen
     * @throws Exception
     */
    @Test(priority = 3)
    public void updateIssueStateWithInvalidState() throws Exception {

        try {
            Map<String, String> query = new HashMap<String, String>();
            query.put("state_event", "in-progress");

            headers = HeaderProvider.getHeaders(header);
            newIssueResponseDTO = gitLabIssueService.UpdateIssue(headers, projectID, issueID, query);
            Assert.assertEquals(newIssueResponseDTO.getStatusCode(), 400);
            Assert.assertTrue(newIssueResponseDTO.getResponse().jsonPath().getString("error").contains("does not have a valid value"));

        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Scenario: Delete an existing Issue
     * @throws Exception
     */
    @Test(priority = 4)
    public void deleteIssue() throws Exception {

        try {

            headers = HeaderProvider.getHeaders(header);
            Response response = gitLabIssueService.deleteIssue(headers, projectID, issueID);
            Assert.assertEquals(response.getStatusCode(), 204);

        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Negative/Edge Case
     * Scenario: Delete a non-existing Issue
     * @throws Exception
     */
    @Test(priority = 5)
    public void deleteNonExistingIssue() throws Exception {

        try {
            headers = HeaderProvider.getHeaders(header);
            Response response = gitLabIssueService.deleteIssue(headers, projectID, issueID);
            Assert.assertEquals(response.getStatusCode(), 404);
            Assert.assertTrue(response.jsonPath().getString("message").equals("404 Issue Not Found"));

        } catch (Exception e) {
            throw e;
        }
    }

}
