package com.abn.gitLab.responses;

import com.abn.gitLab.responses.DTO.*;
import com.abn.gitlab.test.BaseResponseDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;

@Getter @Setter
public class NewIssueResponseDTO extends BaseResponseDTO {


    private int id;
    private int iid;
    private int project_id;
    private String title;
    private Object description;
    private String state;
    private Date created_at;
    private Date updated_at;
    private Object closed_at;
    private Object closed_by;
    private ArrayList<String> labels;
    private Object milestone;
    private ArrayList<Object> assignees;
    private AuthorDTO author;
    private String type;
    private Object assignee;
    private int user_notes_count;
    private int merge_requests_count;
    private int upvotes;
    private int downvotes;
    private Object due_date;
    private boolean confidential;
    private Object discussion_locked;
    private String issue_type;
    private String web_url;
    private TimeStatsDTO time_stats;
    private TaskCompletionStatusDTO task_completion_status;
    private int blocking_issues_count;
    private boolean has_tasks;
    private LinksDTO _links;
    private ReferencesDTO references;
    private String severity;
    private boolean subscribed;
    private Object moved_to_id;
    private Object service_desk_reply_to;
}
