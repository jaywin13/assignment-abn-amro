package com.abn.gitLab.responses.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AuthorDTO {

    private int id;
    private String username;
    private String name;
    private String state;
    private String avatar_url;
    private String web_url;

}
