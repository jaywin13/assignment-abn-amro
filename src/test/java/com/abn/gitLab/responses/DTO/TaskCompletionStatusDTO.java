package com.abn.gitLab.responses.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TaskCompletionStatusDTO {

    private int count;
    private int completed_count;
}
