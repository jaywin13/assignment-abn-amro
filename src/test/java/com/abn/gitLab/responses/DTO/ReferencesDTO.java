package com.abn.gitLab.responses.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ReferencesDTO {

    @JsonProperty("short")
    private String myshort;
    private String relative;
    private String full;
}
