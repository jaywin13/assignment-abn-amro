package com.abn.gitLab.responses.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class LinksDTO {

    private String self;
    private String notes;
    private String award_emoji;
    private String project;
}
