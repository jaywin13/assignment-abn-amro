package com.abn.gitLab.responses.DTO;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TimeStatsDTO {

    private int time_estimate;
    private int total_time_spent;
    private Object human_time_estimate;
    private Object human_total_time_spent;
}
